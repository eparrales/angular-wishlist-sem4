import { Component } from '@angular/core';
import {Observable} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Wishlist-List whit Redux';
  time =  new Observable(observer => {
    setInterval(() => observer.next(new Date().toString()), 1000);
  });

  constructor(public translate: TranslateService) {
    console.log('******** get Translation');
    translate.getTranslation('es').subscribe(x => console.log(`x: ` + JSON.stringify(x)));
    translate.setDefaultLang('es');
  }
}
