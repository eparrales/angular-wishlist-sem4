import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-vuelos-detalle-componentt',
  templateUrl: './vuelos-detalle-componentt.component.html',
  styleUrls: ['./vuelos-detalle-componentt.component.css']
})
export class VuelosDetalleComponenttComponent implements OnInit {
 id: any;
  constructor(private route: ActivatedRoute) {
    route.params.subscribe( params => {this.id = params.id; });
  }

  ngOnInit(): void {
  }

}
